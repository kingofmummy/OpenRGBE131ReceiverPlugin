#-----------------------------------------------------------------------#
# OpenRGB E1.31 Receiver Plugin GitLab CI Configuration                 #
#-----------------------------------------------------------------------#
variables:
  GIT_SUBMODULE_STRATEGY: recursive

.shared_windows_runners:
  tags:
  - shared-windows
  - windows

stages:
  - build

before_script:
 - echo "started by ${GITLAB_USER_NAME}"

#reusable templates
.ccache_init: &ccache_init
  before_script:
    - apt update
    - apt install -y build-essential qtcreator qt5-default qtdeclarative5-dev qtwebengine5-dev libusb-1.0-0-dev libhidapi-dev pkgconf wget git

#-----------------------------------------------------------------------#
# Linux (AppImage) 32-bit Build Target                                  #
#-----------------------------------------------------------------------#
build_linux_32:
  <<: *ccache_init
  image: i386/ubuntu:bionic
  stage: build
  script:
  - export $(dpkg-architecture)
  - ./scripts/build-plugin.sh
  
  artifacts:
    paths:
    - libOpenRGBE131ReceiverPlugin.so.1.0.0
    expire_in: 30 days

#-----------------------------------------------------------------------#
# Linux (AppImage) 64-bit Build Target                                  #
#-----------------------------------------------------------------------#
build_linux_64:
  <<: *ccache_init
  image: ubuntu:bionic
  stage: build
  script:
  - export $(dpkg-architecture)
  - ./scripts/build-plugin.sh
  
  artifacts:
    paths:
    - libOpenRGBE131ReceiverPlugin.so.1.0.0
    expire_in: 30 days

#-----------------------------------------------------------------------#
# Windows (32-bit) Build Target                                         #
#-----------------------------------------------------------------------#
build_windows_32:
  extends:
  - .shared_windows_runners
  stage: build
  script:
  - $esc = "$([char]27)"
  - $count = 0
  - function _unix_tmsec_ { [int64](([datetime]::UtcNow)-(get-date "1/1/1970")).TotalSeconds }
  - function _fold_start_ { param( [string]$TEXT_TAG ) $t=_unix_tmsec_; $global:count += 1; Write-Host -NoNewLine "`r`n`r`nsection_start:${t}:sect_${count}`r${esc}[0K${esc}[33m${TEXT_TAG}${esc}[39m`r`n"; }
  - function _fold_final_ {                            $t=_unix_tmsec_;                     Write-Host -NoNewLine   "`r`n`r`nsection_end:${t}:sect_${count}`r${esc}[0K`r`n"           ; }


  - _fold_start_ 'configuring the msvc environment variables'
  - Push-Location "C:/Program Files (x86)/Microsoft Visual Studio/2019/BuildTools/VC/Auxiliary/Build"
  - '& cmd.exe /C "vcvarsall.bat x86 & set" | Foreach-Object { if ($_ -match "(.*?)=(.*)") { Set-Item -force -path "Env:\$($matches[1])" -value "$($matches[2])" } }'
  - Pop-Location
  - _fold_final_
  
  - _fold_start_ 'downloading precompiled versions of qtbase, qttools (for windeployqt) and jom (for a more parallel nmake)'
  - mkdir _qt
  - mkdir _qt_download
  - Push-Location _qt_download
  - curl.exe -LJ -o qt-base.7z        'https://download.qt.io/online/qtsdkrepository/windows_x86/desktop/qt5_5150/qt.qt5.5150.win32_msvc2019/5.15.0-0-202005150700qtbase-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86.7z' 
  - curl.exe -LJ -o qt-tools.7z       'https://download.qt.io/online/qtsdkrepository/windows_x86/desktop/qt5_5150/qt.qt5.5150.win32_msvc2019/5.15.0-0-202005150700qttools-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86.7z'
  - _fold_final_
  
  - _fold_start_ 'extracting the downloaded qt binaries'
  - 7z x qt-base.7z        '-o../_qt' -y
  - 7z x qt-tools.7z       '-o../_qt' -y
  - _fold_final_
  
  - _fold_start_ 'turn the qt install from enterprise to foss; remove the licensing checks'
  - ${qconfig-pri-folder} = '..\_qt\5.15.0\msvc2019\mkspecs\qconfig.pri'
  - (Get-Content ${qconfig-pri-folder}).replace('QT_EDITION = Enterprise', 'QT_EDITION = OpenSource') | Set-Content ${qconfig-pri-folder}
  - (Get-Content ${qconfig-pri-folder}).replace('QT_LICHECK = licheck.exe', '')                       | Set-Content ${qconfig-pri-folder}
  - Pop-Location
  - _fold_final_
  
  - _fold_start_ 'run qmake and generate the msvc nmake makefile'
  - mkdir _build; cd _build
  - ..\_qt\5.15.0\msvc2019\bin\qmake ..\OpenRGBE131ReceiverPlugin.pro
  - _fold_final_
  
  - _fold_start_ 'start the actual build with jom instead of nmake; for speed'
  - nmake
  - _fold_final_

  - _fold_start_ 'compressing the release folder so that we can upload it as artifact'
  - mv release OpenRGBE131ReceiverPlugin_32
  - ${revision} = ((git rev-parse --short HEAD) | Out-String).Trim()
  - 7z a -mx9 -r -y "OpenRGBE131ReceiverPlugin_32_${revision}.7z" OpenRGBE131ReceiverPlugin_32
  - _fold_final_

  artifacts:
    paths:
    - _build/*.7z
    expire_in: 30 days

#-----------------------------------------------------------------------#
# Windows (64-bit) Build Target                                         #
#-----------------------------------------------------------------------#
build_windows_64:
  extends:
  - .shared_windows_runners
  stage: build
  script:
  - $esc = "$([char]27)"
  - $count = 0
  - function _unix_tmsec_ { [int64](([datetime]::UtcNow)-(get-date "1/1/1970")).TotalSeconds }
  - function _fold_start_ { param( [string]$TEXT_TAG ) $t=_unix_tmsec_; $global:count += 1; Write-Host -NoNewLine "`r`n`r`nsection_start:${t}:sect_${count}`r${esc}[0K${esc}[33m${TEXT_TAG}${esc}[39m`r`n"; }
  - function _fold_final_ {                            $t=_unix_tmsec_;                     Write-Host -NoNewLine   "`r`n`r`nsection_end:${t}:sect_${count}`r${esc}[0K`r`n"           ; }


  - _fold_start_ 'configuring the msvc environment variables'
  - Push-Location "C:/Program Files (x86)/Microsoft Visual Studio/2019/BuildTools/VC/Auxiliary/Build"
  - '& cmd.exe /C "vcvarsall.bat x64 & set" | Foreach-Object { if ($_ -match "(.*?)=(.*)") { Set-Item -force -path "Env:\$($matches[1])" -value "$($matches[2])" } }'
  - Pop-Location
  - _fold_final_
  
  - _fold_start_ 'downloading precompiled versions of qtbase, qttools (for windeployqt) and jom (for a more parallel nmake)'
  - mkdir _qt
  - mkdir _qt_download
  - Push-Location _qt_download
  - curl.exe -LJ -o qt-base.7z        'https://download.qt.io/online/qtsdkrepository/windows_x86/desktop/qt5_5150/qt.qt5.5150.win64_msvc2019_64/5.15.0-0-202005150700qtbase-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z' 
  - curl.exe -LJ -o qt-tools.7z       'https://download.qt.io/online/qtsdkrepository/windows_x86/desktop/qt5_5150/qt.qt5.5150.win64_msvc2019_64/5.15.0-0-202005150700qttools-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z'
  - _fold_final_
  
  - _fold_start_ 'extracting the downloaded qt binaries'
  - 7z x qt-base.7z        '-o../_qt' -y
  - 7z x qt-tools.7z       '-o../_qt' -y
  - _fold_final_
  
  - _fold_start_ 'turn the qt install from enterprise to foss; remove the licensing checks'
  - ${qconfig-pri-folder} = '..\_qt\5.15.0\msvc2019_64\mkspecs\qconfig.pri'
  - (Get-Content ${qconfig-pri-folder}).replace('QT_EDITION = Enterprise', 'QT_EDITION = OpenSource') | Set-Content ${qconfig-pri-folder}
  - (Get-Content ${qconfig-pri-folder}).replace('QT_LICHECK = licheck.exe', '')                       | Set-Content ${qconfig-pri-folder}
  - Pop-Location
  - _fold_final_
  
  - _fold_start_ 'run qmake and generate the msvc nmake makefile'
  - mkdir _build; cd _build
  - ..\_qt\5.15.0\msvc2019_64\bin\qmake ..\OpenRGBE131ReceiverPlugin.pro
  - _fold_final_
  
  - _fold_start_ 'start the actual build with jom instead of nmake; for speed'
  - nmake
  - _fold_final_

  - _fold_start_ 'compressing the release folder so that we can upload it as artifact'
  - mv release OpenRGBE131ReceiverPlugin_64
  - ${revision} = ((git rev-parse --short HEAD) | Out-String).Trim()
  - 7z a -mx9 -r -y "OpenRGBE131ReceiverPlugin_64_${revision}.7z" OpenRGBE131ReceiverPlugin_64
  - _fold_final_

  artifacts:
    paths:
    - _build/*.7z
    expire_in: 30 days

#-----------------------------------------------------------------------#
# MacOS Build Target                                                    #
#-----------------------------------------------------------------------#
build_macos_arm64:
  tags:
  - macos
  stage: build
  script:
  - eval $(/opt/homebrew/bin/brew shellenv)
  - qmake OpenRGBE131ReceiverPlugin.pro
  - make -j16
  - install_name_tool libOpenRGBE131ReceiverPlugin.1.0.0.dylib -change /opt/homebrew/opt/qt@5/lib/QtWidgets.framework/Versions/5/QtWidgets @executable_path/../Frameworks/QtWidgets.framework/Versions/5/QtWidgets
  - install_name_tool libOpenRGBE131ReceiverPlugin.1.0.0.dylib -change /opt/homebrew/opt/qt@5/lib/QtGui.framework/Versions/5/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/5/QtGui
  - install_name_tool libOpenRGBE131ReceiverPlugin.1.0.0.dylib -change /opt/homebrew/opt/qt@5/lib/QtCore.framework/Versions/5/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/5/QtCore
  - codesign --force --verify -s OpenRGB libOpenRGBE131ReceiverPlugin.1.0.0.dylib

  artifacts:
    paths:
    - libOpenRGBE131ReceiverPlugin.1.0.0.dylib
    expire_in: 30 days

  rules:
  - if: '$CI_PROJECT_PATH == "OpenRGBDevelopers/OpenRGBE131ReceiverPlugin"'
    when: on_success
  - if: '$BUILD_MACOS =~ /.+/'
    when: on_success

build_macos_intel:
  tags:
  - macos
  stage: build
  script:
  - eval $(/usr/local/bin/brew shellenv)
  - arch -x86_64 /usr/local/bin/qmake OpenRGBE131ReceiverPlugin.pro
  - arch -x86_64 make -j16
  - arch -x86_64 install_name_tool libOpenRGBE131ReceiverPlugin.1.0.0.dylib -change /usr/local/bin/opt/qt@5/lib/QtWidgets.framework/Versions/5/QtWidgets @executable_path/../Frameworks/QtWidgets.framework/Versions/5/QtWidgets
  - arch -x86_64 install_name_tool libOpenRGBE131ReceiverPlugin.1.0.0.dylib -change /usr/local/bin/opt/qt@5/lib/QtGui.framework/Versions/5/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/5/QtGui
  - arch -x86_64 install_name_tool libOpenRGBE131ReceiverPlugin.1.0.0.dylib -change /usr/local/bin/opt/qt@5/lib/QtCore.framework/Versions/5/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/5/QtCore
  - arch -x86_64 codesign --force --verify -s OpenRGB libOpenRGBE131ReceiverPlugin.1.0.0.dylib
  artifacts:
    paths:
    - libOpenRGBE131ReceiverPlugin.1.0.0.dylib
    expire_in: 30 days

  rules:
  - if: '$CI_PROJECT_PATH == "OpenRGBDevelopers/OpenRGBE131ReceiverPlugin"'
    when: on_success
  - if: '$BUILD_MACOS =~ /.+/'
    when: on_success